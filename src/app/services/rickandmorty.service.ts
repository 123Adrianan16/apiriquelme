import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RickandmortyService {

  //? Escribir URL a la cual se hará la petición.
  public URL = 'https://rickandmortyapi.com/api';

  //? Inyectar el método HTTPCLIENT en el constructor
  constructor( private _http : HttpClient) { }

  //! Petición
  getPersonajes() {

    const uri = `${this.URL}/character`; //Concatenar variables
    return this._http.get(uri); //hacer petición get
  }
}
