import { Component, OnInit } from '@angular/core';
import { RickandmortyService } from 'src/app/services/rickandmorty.service';

@Component({
  selector: 'app-componente-uno',
  templateUrl: './componente-uno.component.html',
  styleUrls: ['./componente-uno.component.css']
})
export class ComponenteUnoComponent implements OnInit {

  public personajes : any[] = []; //setear datos del tipo ninguno
  constructor( private _rickandmorty : RickandmortyService ) {} //heredar al servicio

  ngOnInit(): void { //ciclo de vida del componente
    this.getTodosPersonajes();
  }

  getTodosPersonajes(){
    this.personajes = [];

    this._rickandmorty.getPersonajes()
    .subscribe((data: any) => { //array de objetos de la appi  //metodo subscribe para promesas
      console.log(data);

      this.personajes = data.results;

      console.log('Personajes Seteados', this.personajes);
    })
  }

}
