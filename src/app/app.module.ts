import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { ComponenteUnoComponent } from './component/componente-uno/componente-uno.component';
import { NavBarComponent } from './component/share/nav-bar/nav-bar.component';

//! Invocar el Router
import { RouterModule } from '@angular/router';
import{ ROUTES } from './app.routes';

//! Inportamos el HttpClient
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ComponenteUnoComponent,
    NavBarComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {useHash: true})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
